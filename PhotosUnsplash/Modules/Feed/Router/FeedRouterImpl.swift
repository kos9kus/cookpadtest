//
//  FeedRouterImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class FeedRouterImpl: NSObject, FeedRouter, UIViewControllerTransitioningDelegate {
    
    weak var view: FeedViewController!
    
    func showCategoryModule(completion: @escaping (Int) -> ()) {
        let categoryViewController = CategoryViewController.build()
        categoryViewController.presenter.didChooseCollectionHandler = completion
        categoryViewController.modalPresentationStyle = .overFullScreen
        categoryViewController.transitioningDelegate = self
        view.present(categoryViewController, animated: true)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CategoryPresentAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CategoryDismissAnimator()
    }
}

