//
//  FeedRouter.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol FeedRouter {
    func showCategoryModule(completion: @escaping (_ collectionId: Int) -> ())
}
