//
//  FeedInteractorImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class FeedInteractorImpl: FeedInteractor {
    weak var presenter: FeedPresenterOutput!
    private let service: PhotosService
    
    init(service: PhotosService) {
        self.service = service
    }
    
    // MARK: FeedInteractor
    
    func fetchPhotos() {
        service.fetchPhotos(completion: { [weak self] (photos) in
            DispatchQueue.main.async {
                self?.presenter.reloadData(photos: photos)
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.presenter.handleError(error: error)
            }
        }
    }
    
    func fetchPhotos(collectionId: Int) {
        self.service.fetchPhotos(collectionId: collectionId, completion: { [weak self] (photos) in
            DispatchQueue.main.async {
                self?.presenter.reloadData(photos: photos)
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.presenter.handleError(error: error)
            }
        }
    }
}
