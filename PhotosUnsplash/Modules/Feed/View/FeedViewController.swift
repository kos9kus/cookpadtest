//
//  FeedViewController.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController, FeedViewControllerInput, ViewReloadable, UITableViewDataSource {
    
    var presenter: FeedPresenter!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewIdReady()
    }
    @IBAction func didTapRightNavBarItem(_ sender: Any) {
        presenter.didActionRightNavBarItem()
    }
    
    // MARK: FeedViewControllerInput
    
    func reloadTableView() {
        tableView.reloadData()
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.identifier) as? FeedTableViewCell else {
            fatalError("Didn't find FeedTableViewCell class")
        }
        
        let photo = presenter.photos[indexPath.row]
        cell.setImageUrl(imageUrl: photo.photoUrl)
        
        return cell
    }
}
