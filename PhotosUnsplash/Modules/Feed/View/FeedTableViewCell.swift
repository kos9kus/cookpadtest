//
//  FeedTableViewCell.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell, Identifierable {
    
    @IBOutlet private weak var mainImageView: UIImageView!
    @IBOutlet private weak var spinner: SpinnerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // TODO: It needs to remove that for performance and substitude to ready rounded image from the Image Loader Lib
        mainImageView.clipsToBounds = true
        mainImageView.layer.cornerRadius = mainImageView.bounds.size.height / 2
    }
    
    private func enableSpinner(enable: Bool) {
        spinner.isHidden = !enable
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        enableSpinner(enable: true)
    }
    
    func setImageUrl(imageUrl: String) {
        spinner.startRotatingAnimation()
        mainImageView.imageLoader.image(url: imageUrl, placeholder: UIImage(named: "imagePlaceholder")!) { [weak self] in
            self?.enableSpinner(enable: false)
        }
    }

}
