//
//  FeedAssembly.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class FeedAssembly: NSObject {
    @IBOutlet weak var view: FeedViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let interactor = FeedInteractorImpl(service: PhotosServiceBuilder.build())
        let router = FeedRouterImpl()
        router.view = view
        let presenter = FeedPresenterImpl()
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        view.presenter = presenter
        
    }
}
