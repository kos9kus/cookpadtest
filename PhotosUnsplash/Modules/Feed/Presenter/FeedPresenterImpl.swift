//
//  FeedPresenterImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class FeedPresenterImpl: FeedPresenter, FeedPresenterOutput {
    
    weak var view: FeedViewControllerInput!
    var router: FeedRouter!
    var interactor: FeedInteractor!
    private(set) var photos: [Photo] = []
    
    // MARK: FeedPresenter
    
    func viewIdReady() {
        interactor.fetchPhotos()
        view.enableSpinner(enable: true)
    }
    
    func didActionRightNavBarItem() {
        router.showCategoryModule { [weak self] (collectionId) in
            guard let `self` = self else {
                return
            }
            self.interactor.fetchPhotos(collectionId: collectionId)
            self.view.enableSpinner(enable: true)
        }
    }
    
    // MARK: FeedPresenterOutput
    
    func reloadData(photos: [Photo]) {
        self.photos = photos
        view.enableSpinner(enable: false)
        view.reloadTableView()
    }
    
    func handleError(error: ErrorDescriptor) {
        view.enableSpinner(enable: false)
    }
}
