//
//  FeedPresenter.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol FeedPresenter: class {
    var photos: [Photo] { get }
    
    func viewIdReady()
    func didActionRightNavBarItem()
}

protocol FeedPresenterOutput: class {
    func reloadData(photos: [Photo])
    func handleError(error: ErrorDescriptor)
}
