//
//  CategoryViewController.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController, CategoryViewInput, Identifierable, Assemblable, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var presenter: CategoryPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewIsReady()
    }
    
    // MARK: CategoryViewInput
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
    // MARK: UITableViewDataSource UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.collections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.identifier) as? CategoryTableViewCell else {
            fatalError("Didn't find CategoryTableViewCell class")
        }
        
        let collection = presenter.collections[indexPath.row]
        cell.configureCell(imageUrl: collection.coverPhoto.photoUrl, name: collection.title)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        presenter.didActionSelectCell(at: indexPath.row)
    }
}
