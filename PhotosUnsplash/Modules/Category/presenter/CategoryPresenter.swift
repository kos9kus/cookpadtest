//
//  CategoryPresenter.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol CategoryPresenter: class {
    var collections: [PhotoCollection] { get }
    var didChooseCollectionHandler: (_ collectionId: Int) -> () { get set }
    func viewIsReady()
    func didActionSelectCell(at index: Int)
}

protocol CategoryPresenterOutput: class {
    func reloadData(collections: [PhotoCollection])
    func handleError(error: ErrorDescriptor)
}
