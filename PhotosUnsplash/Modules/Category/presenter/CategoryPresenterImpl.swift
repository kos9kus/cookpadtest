//
//  CategoryPresenterImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class CategoryPresenterImpl: CategoryPresenter, CategoryPresenterOutput {
    
    var didChooseCollectionHandler: (Int) -> () = { _ in }
    weak var view: CategoryViewInput!
    var interactor: CategoryInteractor!
    var router: CategoryRouter!
    private(set) var collections: [PhotoCollection] = []
    
    // MARK: CategoryPresenterOutput
    
    func reloadData(collections: [PhotoCollection]) {
        self.collections = collections
        view.reloadTableView()
        self.view.enableSpinner(enable: false)
    }
    
    func handleError(error: ErrorDescriptor) {
        self.view.enableSpinner(enable: false)
    }
    
    // MARK: CategoryPresenter
    
    func viewIsReady() {
        self.interactor.fetchCollections()
        self.view.enableSpinner(enable: true)
    }
    
    func didActionSelectCell(at index: Int) {
        let collection = collections[index]
        didChooseCollectionHandler(collection.id)
        router.dismissModule()
    }
    
    
}
