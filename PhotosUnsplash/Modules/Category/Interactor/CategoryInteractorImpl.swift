//
//  CategoryInteractorImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class CategoryInteractorImpl: CategoryInteractor {
    weak var presenter: CategoryPresenterOutput!
    
    private let service: PhotosService
    
    init(service: PhotosService) {
        self.service = service
    }
    
    // MARK: CategoryInteractor
    
    func fetchCollections() {
        service.fetchCollections(completion: { [weak self] (collections) in
            DispatchQueue.main.async {
                self?.presenter.reloadData(collections: collections)
            }
        }) { [weak self] (error) in
            DispatchQueue.main.async {
                self?.presenter.handleError(error: error)
            }
        }
    }
}
