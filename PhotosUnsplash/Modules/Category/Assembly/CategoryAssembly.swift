//
//  CategoryAssembly.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class CategoryAssembly: NSObject {
    
    @IBOutlet weak var view: CategoryViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let interactor = CategoryInteractorImpl(service: PhotosServiceBuilder.build())
        let router = CategoryRouterImpl()
        router.view = view
        let presenter = CategoryPresenterImpl()
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
        presenter.view = view
        view.presenter = presenter
    }
}
