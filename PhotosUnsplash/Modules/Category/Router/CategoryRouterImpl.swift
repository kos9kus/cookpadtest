//
//  CategoryRouterImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class CategoryRouterImpl: CategoryRouter {
    weak var view: CategoryViewController!
    
    func dismissModule() {
        view.dismiss(animated: true)
    }
}
