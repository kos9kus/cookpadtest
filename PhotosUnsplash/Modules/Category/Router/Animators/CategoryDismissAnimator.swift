//
//  CategoryDismissAnimator.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 13/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class CategoryDismissAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var dimmingView: UIView = {
        let view = UIView.init()
        view.backgroundColor = UIColor.black
        return view
    }()
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let categoryViewController = transitionContext.viewController(forKey: .from) else {
            fatalError()
        }
        
        let initialFrame = transitionContext.initialFrame(for: categoryViewController)
        var finalFrame = initialFrame
        finalFrame.origin.x = finalFrame.size.width
        
        dimmingView.frame = transitionContext.containerView.bounds
        dimmingView.alpha = 0.35
        
        let containerView = transitionContext.containerView
        containerView.addSubview(dimmingView)
        containerView.addSubview(categoryViewController.view)
        UIView.animate(withDuration:self.transitionDuration(using: transitionContext), animations: {
            categoryViewController.view.frame = finalFrame
            self.dimmingView.alpha = 0
        }) { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
