//
//  CategoryPresentAnimator.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 13/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class CategoryPresentAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var dimmingView: UIView = {
        let view = UIView.init()
        view.backgroundColor = UIColor.black
        return view
    }()
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let categoryViewController = transitionContext.viewController(forKey: .to) else {
            fatalError()
        }
        
        let finalFrame = transitionContext.finalFrame(for: categoryViewController)
        var initialFrame = finalFrame
        initialFrame.origin.x = initialFrame.size.width
        categoryViewController.view.frame = initialFrame
        
        dimmingView.frame = finalFrame
        dimmingView.alpha = 0
        
        let containerView = transitionContext.containerView
        containerView.addSubview(dimmingView)
        containerView.addSubview(categoryViewController.view)
        UIView.animate(withDuration:self.transitionDuration(using: transitionContext), animations: {
            categoryViewController.view.frame = finalFrame
            self.dimmingView.alpha = 0.35
        }) { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    

}
