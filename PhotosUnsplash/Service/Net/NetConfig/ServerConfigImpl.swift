//
//  ServerConfigImpl.swift
//  PhotosUnsplashImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 23/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ConfigMethodName {
    var method: String { get }
}

struct ServerConfigImpl: ServerConfig {
    
    private let configMethod: ConfigMethodName
    
    init(configMethod: ConfigMethodName) {
        self.configMethod = configMethod
    }
    
    var clientId: String {
        return "f1ce6aad38c81fce0f445778e66f9b4d88d6db04897202c451a1e6fc214c6540"
    }
    
    var method: String {
        return configMethod.method
    }
    
    var query: String {
        return "client_id=" + clientId + "&per_page=30"
    }
    
    var serverDomain: String {
        return "api.unsplash.com"
    }
    
    var scheme: String {
        return "https"
    }
}
