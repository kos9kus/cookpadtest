//
//  ProcedureMethod.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

enum PhotosUnsplashProcedureMethod: ConfigMethodName {
    case collections
    case collectionPhotos(id: Int)
    case randomPhotos
    
    var method: String {
        switch self {
        case .randomPhotos:
            return "/photos"
        case .collections:
            return "/collections"
        case .collectionPhotos(let id):
            return "/collections/\(id)/photos"
        }
    }
}
