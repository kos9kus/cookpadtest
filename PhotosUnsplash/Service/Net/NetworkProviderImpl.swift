//
//  NetworkProviderImpl.swift
//  PhotosUnsplashImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 23/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class NetworkProviderImpl: NetworkProvider {
    typealias Request = URLRequest
    
    private let session: URLSession = {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.requestCachePolicy = .reloadIgnoringLocalCacheData
        let session = URLSession.init(configuration: sessionConfig)
        return session
    }()
    
    var task: URLSessionDataTask?
    var cancelled = false
    
    func makeRequest(request: Request, completion: @escaping (Data) -> (), failure: @escaping (Network<NetworkProviderImpl>.NetworkErrorResponse) -> ()) {
        cancelled = false
        task = session.dataTask(with: request) { [weak self] (data, response, error) in
            guard let `self` = self, self.cancelled == false else {
                return
            }
            if let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                completion(data)
            } else {
                self.handleBadResponse(response: response, error: error, failure: failure)
            }
        }
        task?.resume()
    }
    
    func cancel() {
        if cancelled {
            return
        }
        cancelled = true
        task?.cancel()
    }
    
    private func handleBadResponse(response: URLResponse?, error: Error?, failure: @escaping (Network<NetworkProviderImpl>.NetworkErrorResponse) -> ()) {
        if let httpResponse = response as? HTTPURLResponse {
            failure(.badResponse(httpResponse))
        } else if let error = error {
            failure(.error(error))
        } else {
            failure(.error(MainNetworkProviderError()))
        }
    }
    
    struct MainNetworkProviderError: Error {
        var localizedDescription: String {
            return NSLocalizedString("Undefined network error", comment: "")
        }
    }
}
