//
//  ParserImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class ParserImpl: Parser {
    
    func parsePhotoCollections(data: Data) throws -> [PhotoCollection] {
        let collections: [PhotoCollectionImpl] = try self.parse(data: data)
        return collections
    }
    
    func parsePhotos(data: Data) throws -> [Photo] {
        let photos: [PhotoImpl] = try self.parse(data: data)
        return photos
    }
    
    struct ParserErrorDescriptor: ErrorDescriptor {
        var verboseMessage: String
        
        init(verboseMessage: String) {
            self.verboseMessage = verboseMessage
        }
    
        var title: String {
            return "Parser error"
        }
        
        var message: String {
            return "Unable to parse"
        }
        
        
    }
    
    private func parse<T>(data: Data) throws -> T where T : Decodable {
        do {
            return try jsonDecoder.decode(T.self, from: data)
        } catch let e {
            throw ParserErrorDescriptor(verboseMessage: e.localizedDescription)
        }
    }
    
    private let jsonDecoder = JSONDecoder()
}
