//
//  Parser.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol Parser {
    func parsePhotoCollections(data: Data) throws -> [PhotoCollection]
    func parsePhotos(data: Data) throws -> [Photo]
}
