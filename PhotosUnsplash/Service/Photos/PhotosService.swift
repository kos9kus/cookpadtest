//
//  PhotosService.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol PhotosService {
    func fetchPhotos(completion: @escaping ([Photo]) -> (), failure: @escaping (ErrorDescriptor) -> ())
    func fetchPhotos(collectionId: Int, completion: @escaping ([Photo]) -> (), failure: @escaping (ErrorDescriptor) -> ())
    func fetchCollections(completion: @escaping ([PhotoCollection]) -> (), failure: @escaping (ErrorDescriptor) -> ())
}
