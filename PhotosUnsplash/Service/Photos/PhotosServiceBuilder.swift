//
//  PhotosServiceBuilder.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class PhotosServiceBuilder {
    
    private init() {}
    
    static func build() -> PhotosService {
        let parser = ParserImpl()
        let networkProvider = NetworkProviderImpl()
        let network = Network(networkProvider: networkProvider)
        return PhotosServiceImpl(parser: parser, network: network)
    }
}
