//
//  PhotoCollectionImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

struct PhotoCollectionImpl: PhotoCollection, Decodable {
    var title: String
    
    let id: Int
    
    var coverPhoto: Photo {
        return coverPhotoDecodable
    }
    
    enum PhotoCollectionKey: String, CodingKey {
        case id
        case title
        case cover_photo
    }
    
    private let coverPhotoDecodable: PhotoImpl
    
    init(from decoder: Decoder) throws {
        let allValues = try decoder.container(keyedBy: PhotoCollectionKey.self)
        id = try allValues.decode(Int.self, forKey: .id)
        title = try allValues.decode(String.self, forKey: .title)
        coverPhotoDecodable = try allValues.decode(PhotoImpl.self, forKey: .cover_photo)
    }
}
