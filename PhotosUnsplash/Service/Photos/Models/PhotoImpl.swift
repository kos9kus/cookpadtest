//
//  PhotoImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation


struct PhotoImpl: Decodable, Photo {
    let photoUrl: String
    
    enum PhotoKey: String, CodingKey {
        case urls
    }
    
    enum PhotoUrlsKey: String, CodingKey {
        case regular
    }
    
    init(from decoder: Decoder) throws {
        let allValues = try decoder.container(keyedBy: PhotoKey.self)
        let urls = try allValues.nestedContainer(keyedBy: PhotoUrlsKey.self, forKey: .urls)
        photoUrl = try urls.decode(String.self, forKey: .regular)
    }
    
}
