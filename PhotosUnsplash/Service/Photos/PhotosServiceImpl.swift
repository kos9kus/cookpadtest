//
//  PhotosServiceImpl.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class PhotosServiceImpl<Provider: NetworkProviderImpl>: PhotosService {
    
    
    
    private let parser: Parser
    private let network: Network<Provider>
    
    init(parser: Parser, network: Network<Provider>) {
        self.parser = parser
        self.network = network
    }
    
    func fetchPhotos(completion: @escaping ([Photo]) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
        let method = PhotosUnsplashProcedureMethod.randomPhotos
        let config = ServerConfigImpl(configMethod: method)
        let components = NSURLComponents.request(serverConfig: config)
        self.fetchPhotos(components: components, completion: completion, failure: failure)
    }
    
    func fetchPhotos(collectionId: Int, completion: @escaping ([Photo]) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
        let method = PhotosUnsplashProcedureMethod.collectionPhotos(id: collectionId)
        let config = ServerConfigImpl(configMethod: method)
        let components = NSURLComponents.request(serverConfig: config)
        self.fetchPhotos(components: components, completion: completion, failure: failure)
    }
    
    func fetchCollections(completion: @escaping ([PhotoCollection]) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
        let method = PhotosUnsplashProcedureMethod.collections
        let config = ServerConfigImpl(configMethod: method)
        let components = NSURLComponents.request(serverConfig: config)
        guard let urlString = components.url?.absoluteString, let url = URL(string: urlString) else {
            fatalError("Invalid Net config")
        }
        let request = URLRequest(url: url)
        self.network.makeRequest(request: request, completion: { [weak self] (data) in
            guard let `self` = self else {
                return
            }
            do {
                let photoCollections: [PhotoCollection] = try self.parser.parsePhotoCollections(data: data)
                completion(photoCollections)
            } catch let e as ErrorDescriptor {
                failure(e)
            } catch let e {
                print(e)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    private func fetchPhotos(components: NSURLComponents, completion: @escaping ([Photo]) -> (), failure: @escaping (ErrorDescriptor) -> ()) {
        guard let urlString = components.url?.absoluteString, let url = URL(string: urlString) else {
            fatalError("Invalid Net config")
        }
        let request = URLRequest(url: url)
        self.network.makeRequest(request: request, completion: { [weak self] (data) in
            guard let `self` = self else {
                return
            }
            do {
                let photos: [Photo] = try self.parser.parsePhotos(data: data)
                completion(photos)
            } catch let e as ErrorDescriptor {
                failure(e)
            } catch let e {
                print(e)
            }
        }) { (error) in
            failure(error)
        }
    }
}
