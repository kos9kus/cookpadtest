//
//  PhotosUnsplashImageLoader.swift
//  PhotosUnsplashImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol ImageLoader {
    func image(url: String, placeholder: UIImage, completion: @escaping () -> ())
}
