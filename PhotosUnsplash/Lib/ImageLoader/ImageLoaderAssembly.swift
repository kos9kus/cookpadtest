//
//  ImageLoaderAssembly.swift
//  PhotosUnsplashImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

struct ImageLoaderAssembly {
    static func assembly(imageView: UIImageView) -> ImageLoader {
        let downloaderService = ImageLoaderDownloaderServiceImpl.build()
        return ImageLoaderImpl(imageView: imageView, downloader: downloaderService)
    }
}
