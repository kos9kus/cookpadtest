//
//  ParserImpl.swift
//  PhotosUnsplashImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 23/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

struct ParserErrorDescriptor: ErrorDescriptor {
    var title: String {
        return "Parser error"
    }
    
    var message: String {
        return "Corrupted data"
    }
}

class ImageLoaderParserImpl: ImageLoaderParser {
    func parse(data: Data) throws -> UIImage {
        if let image = UIImage(data: data) {
            return image
        }
        throw ParserErrorDescriptor()
    }
}
