//
//  ImageLoaderCache.swift
//  PhotosUnsplashImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol ImageLoaderCache {
    func loadImage(name: String) -> UIImage?
    func setImage(image: UIImage, name: String) throws
}
