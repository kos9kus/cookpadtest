//
//  ImageLoaderCacheImpl.swift
//  PhotosUnsplashImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class ImageLoaderCacheImpl: ImageLoaderCache {
    
    struct ImageLoaderCacheSettings {
        let numberOfItems: Int
        let diskCacheSize: Int
    }
    
    private let settings: ImageLoaderCacheSettings
    private let queue = DispatchQueue(label: "ImageLoaderCacheImpl")
    let path: String = NSTemporaryDirectory().appending("images/")
    
    init(settings: ImageLoaderCacheSettings) {
        self.settings = settings
        try! FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true)
    }
    
    var localCache: Dictionary<String, UIImage> = [:]
    
    func setImage(image: UIImage, name: String) throws {
        try queue.sync {
            self.clearIfNeeded()
            
            localCache[name] = image
            if let imageData = image.pngData() {
                try imageData.write(to: self.pathImage(name: name))
            }
        }
    }
    
    func loadImage(name: String) -> UIImage? {
        var localImage: UIImage?
        queue.sync {
            localImage = localCache[name]
        }
        return localImage ?? UIImage(contentsOfFile: self.pathImage(name: name))
    }
    
    private func pathImage(name: String) -> URL {
        let pathString = path + name + ".png"
        return URL(fileURLWithPath: pathString)
    }
    
    private func pathImage(name: String) -> String {
        return path + name + ".png"
    }
    
    private func clearIfNeeded() {
        if localCache.count > settings.numberOfItems {
            localCache.removeAll()
        }
        
        do {
            let allDiskCacheSize = try self.remainDiskSize()
            if settings.diskCacheSize < allDiskCacheSize {
                self.clearDisk()
            }
        } catch let exp {
            print(exp)
            self.clearDisk()
        }
        
    }
    
    private func clearDisk() {
        let fileManager = FileManager.default
        do {
            let files = try fileManager.contentsOfDirectory(atPath: path)
            for file in files {
                try fileManager.removeItem(atPath: path + file)
            }
        } catch let exp {
            print(exp)
        }
    }
    
    private func remainDiskSize() throws -> UInt64 {
        let fileManager = FileManager.default
        var accumulatedSize: UInt64 = 0
        let files = try fileManager.contentsOfDirectory(atPath: path)
        for file in files {
            let fileAttributes = try fileManager.attributesOfItem(atPath: path + file)
            if let fileSize = fileAttributes[FileAttributeKey.size] as? UInt64 {
                accumulatedSize += fileSize
            }
        }
        return accumulatedSize
    }
}
