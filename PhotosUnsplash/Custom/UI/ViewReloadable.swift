//
//  ViewReloadable.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol ViewReloadable {
    func enableSpinner(enable: Bool)
}

extension ViewReloadable {
    func enableSpinner(enable: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = enable
    }
}
