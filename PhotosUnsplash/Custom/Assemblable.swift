//
//  Assemblable.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol Assemblable {
    static func build() -> Self
}

extension Assemblable where Self: Identifierable {
    static func build() -> Self {
        guard let vc = UIStoryboard(name: Self.identifier, bundle: nil).instantiateInitialViewController() as? Self else {
            fatalError()
        }
        
        return vc
    }
}
