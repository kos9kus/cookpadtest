//
//  Identifierable.swift
//  PhotosUnsplash
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol Identifierable {
	static var identifier: String { get }
}

extension Identifierable where Self: UIViewController {
	static var identifier: String {
		return String(describing: self)
	}
}

extension Identifierable where Self: UIView {
	static var identifier: String {
		return String(describing: self)
	}
}

