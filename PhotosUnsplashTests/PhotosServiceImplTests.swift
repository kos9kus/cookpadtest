//
//  PhotosServiceImplTests.swift
//  PhotosUnsplashTests
//
//  Created by KONSTANTIN KUSAINOV on 12/01/2019.
//  Copyright © 2019 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
@testable import PhotosUnsplash

class PhotosServiceImplTests: XCTestCase {
    
    func testFetchRandomPhotos() {
        let service = PhotosServiceBuilder.build()
        let exp = self.expectation(description: "testFetchRandomPhotos")
        service.fetchPhotos(completion: { (photos) in
            XCTAssertTrue(photos.count > 0)
            exp.fulfill()
        }) { (error) in
            XCTFail(error.localizedDescription)
        }
        
        waitForExpectations(timeout: 3)
    }
    
    func testFetchCollections() {
        let service = PhotosServiceBuilder.build()
        let exp = self.expectation(description: "testFetchCollections")
        service.fetchCollections(completion: { (collections) in
            XCTAssertTrue(collections.count > 0)
            exp.fulfill()
        }) { (error) in
            XCTFail(error.localizedDescription)
        }
        
        waitForExpectations(timeout: 3)
    }
    
    func testFetchPhotos() {
        let service = PhotosServiceBuilder.build()
        let collectionId = 1099399
        let exp = self.expectation(description: "testFetchPhotos")
        service.fetchPhotos(collectionId: collectionId, completion: { (photos) in
            XCTAssertTrue(photos.count > 0)
            exp.fulfill()
        }) { (error) in
            XCTFail(error.localizedDescription)
        }
        
        waitForExpectations(timeout: 3)
    }

}
